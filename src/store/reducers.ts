import { connectRouter } from 'connected-react-router';
import { Action, combineReducers } from 'redux';


const createRootReducer = (history: any) => combineReducers({
  router: connectRouter(history),
  usersReducer
})

const usersReducer = (state = [], action: Action) => {
  return state;
}

export default createRootReducer;