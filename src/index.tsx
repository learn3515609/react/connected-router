import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import { Provider } from 'react-redux'
import { Route, Switch } from 'react-router'
import { Link } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import configureStore, { history } from './store/store'

const store = configureStore({})

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <Provider store={store}>
    <ConnectedRouter history={history}> { /* place ConnectedRouter under Provider */ }
      <> { /* your usual react-router v4/v5 routing */ }
        <Switch>
          <Route exact path="/" component={() => <div>Match</div>} />
          <Route component={() => <div>Miss</div>} />

        </Switch>
          <Link to="/">main</Link>
          <Link to="/s">inner</Link>
      </>
    </ConnectedRouter>
  </Provider>
);
